<?php
namespace app\models;
use framework\Model as Model;
use framework\FlashMessages as Mess;

class UserModel extends Model
{
	// PDO connection - aceasta linie trebuie sa fie in fiecare model
	private $pdo;

	// suprascrierea constructorului din parinte - linia asta trebuie sa fie in fiecare model
	public function __construct(){
		parent::__construct();
		$this->pdo = parent::$_pdo;
	}	

	private function validate($post)
	{		
		$validate = true;
		if(empty($post['firstName']) || !ctype_alpha($post['firstName'])){
			Mess::setMess('firstname', 'Invalid FistName');
			$validate = false;
		}	
		if(empty($post['lastName']) || !ctype_alpha($post['lastName'])){
			Mess::setMess('lastname', 'Invalid LastName');
			$validate = false;
		}
		if(empty($post['email']) || !filter_var($post['email'], FILTER_VALIDATE_EMAIL)){
			Mess::setMess('email', 'Invalid Email');
			$validate = false;
		}	

		return $validate;
	}

	// Get a list of users
	public function listUser()
	{		
		return $this->pdo->query('SELECT * FROM users');
	}	

	public function update($id, $post)
	{			
		if(!$this->validate($post)){			
			return false;			
		}

		$set = 'firstname = :firstname, lastname = :lastname, email = :email';
		$sql = $this->pdo->prepare("UPDATE users SET $set  WHERE id = :id");

		return $sql->execute(array(
			':id' => $id, 
			':firstname' => $post['firstName'],
			':lastname' => $post['lastName'],
			':email' => $post['email']));				
	}
}