<table cellpadding="2" cellspacing="0" border="1">
	<tr>
		<td>FirstName</td>
		<td>LastName</td>
		<td>Email</td>
		<td>Actions</td>
	</tr>
	<?php foreach($vars['users'] as $user): ?>
	<tr>
		<td><?php echo $user['firstname']; ?></td>
		<td><?php echo $user['lastname']; ?></td>
		<td><?php echo $user['email']; ?></td>
		<td>
			<a href="index.php?c=user&amp;a=edit&amp;id=<?php echo $user['id']; ?>">Edit</a> | 
			<a href="#delete_user" id="delete_user" onclick = "delete_user('<?php echo $user['id']; ?>'); return false;">Delete</a>
		</td>
	</tr>	
	<?php endforeach; ?>	
</table>