<?php

// Configurari ce tin de aplicatie - e.g. Blog
return array(
	'charset' => 'utf-8',
	'theme' => 'blog_theme',	
	'database' => array(
		'host' => 'localhost',
		'name' => 'proiect_php_adv',
		'username' => 'root',
		'password' => 'parola_0',
		'options' => array(
				PDO::ATTR_ERRMODE => PDO::ERRMODE_EXCEPTION,
			),
		),
	// environment - dev|prod
	'env' => 'dev',
);